import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import { paginateModal } from 'lib/utils'
import MediaPicker from 'components/Media/MediaPicker.js'

const MediaPickerContainer = ({ media, mediaPage, value, handleOnChange, currentPage, totalPages }) => {
  return <MediaPicker media={media} mediaPage={mediaPage} value={value} handleOnChange={handleOnChange} currentPage={currentPage} totalPages={totalPages} />
}

MediaPickerContainer.propTypes = {
  media: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  const { docs, totalPages, currentPage } = paginateModal(state, props, state.firestore.ordered.media, state.pagination.modalPage)
  return {
    media: state.firestore.ordered.media,
    mediaPage: docs,
    totalPages,
    currentPage
  }
}

export default compose(
  firestoreConnect([{
    collection: 'media',
    orderBy: ['createdDate', 'desc'],
  }]),
  connect(mapStateToProps)
)(MediaPickerContainer)
