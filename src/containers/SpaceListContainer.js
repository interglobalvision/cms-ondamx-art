import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import SpaceList from 'components/Space/SpaceList.js'
import { listContainerPaginateProps } from 'lib/utils'

const SpaceListContainer = ({ spaces, totalPages, currentPage }) => {
  // In this case, we dont do isLoaded/isEmpty in the container
  // we leverage that to <Spaces /> because theres things to show
  // even while loading
  return <SpaceList spaces={spaces} totalPages={totalPages} currentPage={currentPage} />
}

SpaceListContainer.propTypes = {
  spaces: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  const { docs, totalPages, currentPage } = listContainerPaginateProps(state, props, state.firestore.ordered.spaces)
  return {
    spaces: docs,
    totalPages,
    currentPage
  }
}

export default compose(
  firestoreConnect([{
    collection: 'spaces',
    orderBy: ['createdDate', 'desc'],
  }]),
  connect(mapStateToProps)
)(SpaceListContainer)
