import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import EventList from 'components/Event/EventList.js'
import { listContainerPaginateProps } from 'lib/utils'

const EventListContainer = ({ events, totalPages, currentPage }) => {
  // In this case, we dont do isLoaded/isEmpty in the container
  // we leverage that to <Events /> because theres things to show
  // even while loading
  return <EventList events={events} totalPages={totalPages} currentPage={currentPage} />
}

EventListContainer.propTypes = {
  events: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  const { docs, totalPages, currentPage } = listContainerPaginateProps(state, props, state.firestore.ordered.events)
  return {
    events: docs,
    totalPages,
    currentPage
  }
}

export default compose(
  firestoreConnect([{
    collection: 'events',
    orderBy: ['createdDate', 'desc'],
  }]),
  connect(mapStateToProps)
)(EventListContainer)
