import React from 'react'
import HeaderContainer from 'containers/HeaderContainer'
import MainContainer from 'containers/MainContainer'
import styles from 'lib/styles.css'

const App = () => {
  return (
    <div className='App'>
      <HeaderContainer/>
      <MainContainer/>
    </div>
  )
}

export default App
