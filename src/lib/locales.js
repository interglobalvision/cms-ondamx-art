export const locales = ['es', 'en']

export const generatePerLocale = (item) => {
  let localesAssigned =  {}
  locales.forEach( locale => {
    localesAssigned[locale] = item
  })
  return localesAssigned
}
