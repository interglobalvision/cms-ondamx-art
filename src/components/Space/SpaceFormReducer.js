import { generatePerLocale } from 'lib/locales'
import { ParseEditorContent, emptyEditorState } from 'lib/utils'

export const SPACE_FORM_SET_FIELD_VALUE = 'SPACE_FORM_SET_FIELD_VALUE'
export const SPACE_FORM_SET_LOCALIZED_FIELD_VALUE = 'SPACE_FORM_SET_LOCALIZED_FIELD_VALUE'
export const SPACE_FORM_SET_CHECKING_VALIDATION = 'SPACE_FORM_SET_CHECKING_VALIDATION'
export const SPACE_FORM_SET_ERROR = 'SPACE_FORM_SET_ERROR'
export const SPACE_FORM_RESET_ERROR = 'SPACE_FORM_RESET_ERROR'
export const SPACE_FORM_SET_LOADING = 'SPACE_FORM_SET_LOADING'
export const SPACE_FORM_RESET = 'SPACE_FORM_RESET'
export const SPACE_FORM_LOAD_DATA = 'SPACE_FORM_LOAD_DATA'

// Post initial state
const postContentInitialState = {
  mainContent: ParseEditorContent(emptyEditorState),
  seoTitle: '',
  seoDescription: '',
}

// This is our initial state
export const initialState = {
  formValues: {
    name: '',
    localizedContent: generatePerLocale(postContentInitialState),
    coverImage: {},
    location: {
      address: {
        street: '',
        number: '',
        extra: '',
        neighborhood: '',
        city: '',
        state: '',
        country: '',
        postalCode: '',
      },
      lat: 0,
      lon: 0,
    },
    phone: '',
    email: '',
    website: '',
    instagram: '',
    hours: {
      monday: [null, null],
      tuesday: [null, null],
      wednesday: [null, null],
      thursday: [null, null],
      friday: [null, null],
      saturday: [null, null],
      sunday: [null, null],
    },
    appointment: 'default',
    status: 'draft',
    active: true,
    type: 'gallery',
    keywords: [],
    relatedTo: [],
    relatedExternal: [],
    languages: {
      en: true,
      es: true,
    },
    bookmarkCount: 0,
    bookmarks: [],
  },
  isCheckingValidation: false,
  error: false,
  loading: false,
}

const SpaceFormReducer = (state = initialState, action) => {
  switch (action.type) {
    case SPACE_FORM_SET_FIELD_VALUE:
      const { formValues } = state
      return {
        ...state,
        formValues: {
          ...formValues,
          [action.field]: action.data,
        }
      }
    case SPACE_FORM_SET_LOCALIZED_FIELD_VALUE:
      return {
        ...state,
        formValues: {
          ...state.formValues,
          localizedContent: localizedContentReducer(state.formValues.localizedContent, action)
        }
      }
    case SPACE_FORM_SET_CHECKING_VALIDATION:
      return {
        ...state,
        isCheckingValidation: true,
      }
    case SPACE_FORM_SET_ERROR: {
      return {
        ...state,
        error: action.data || false,
      }
    }
    case SPACE_FORM_RESET_ERROR: {
      return {
        ...state,
        error: false,
      }
    }
    case SPACE_FORM_SET_LOADING: {
      return {
        ...state,
        loading: action.data,
      }
    }
    case SPACE_FORM_RESET: {
      return initialState
    }
    default:
      return state
  }
}

const localizedContentReducer = (state = generatePerLocale(postContentInitialState), action) => {
  switch (action.type) {
    case SPACE_FORM_SET_LOCALIZED_FIELD_VALUE:
      return {
        ...state,
        [action.locale]: {
          ...state[action.locale],
          [action.field]: action.data,
        }
      }
    default:
      return state
  }
}

export default SpaceFormReducer
