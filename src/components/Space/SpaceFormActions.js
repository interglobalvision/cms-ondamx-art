import {
  SPACE_FORM_SET_FIELD_VALUE,
  SPACE_FORM_SET_LOCALIZED_FIELD_VALUE,
  SPACE_FORM_SET_CHECKING_VALIDATION,
  SPACE_FORM_SET_ERROR,
  SPACE_FORM_RESET_ERROR,
  SPACE_FORM_SET_LOADING,
  SPACE_FORM_LOAD_DATA,
  SPACE_FORM_RESET,
} from './SpaceFormReducer'

export const setFieldValue = (field, data) => {
  return {
    type: SPACE_FORM_SET_FIELD_VALUE,
    field,
    data,
  }
}

export const setLocalizedFieldValue = (field, locale, data) => {
  return {
    type: SPACE_FORM_SET_LOCALIZED_FIELD_VALUE,
    field,
    locale,
    data,
  }
}

export const setCheckingValidation = () => {
  return {
    type: SPACE_FORM_SET_CHECKING_VALIDATION,
  }
}

export const setSpaceError = (error = null) => {
  return {
    type: SPACE_FORM_SET_ERROR,
    data: error,
  }
}

export const resetSpaceError = () => {
  return {
    type: SPACE_FORM_RESET_ERROR,
  }
}

export const setLoading = loading => {
  return {
    type: SPACE_FORM_SET_LOADING,
    data: loading,
  }
}

export const loadSpaceData = data => {
  return {
    type: SPACE_FORM_LOAD_DATA,
    data,
  }
}

export const resetSpaceForm = () => {
  return {
    type: SPACE_FORM_RESET,
  }
}
