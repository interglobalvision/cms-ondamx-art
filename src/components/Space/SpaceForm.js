import React, { useReducer } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import InputGroup from 'react-bootstrap/InputGroup'
import Alert from 'react-bootstrap/Alert'
import RichEditor from 'components/Fields/RichEditor/RichEditor'
import Languages from 'components/Fields/Languages'
import LocationField from 'components/Fields/Location'
import HoursField from 'components/Fields/Hours'
import MediaPicker from 'components/Media/MediaPicker'
import MediaPickerContainer from 'containers/MediaPickerContainer'
import RelatedInternalContent from 'components/Fields/RelatedInternalContent'
import { convertToRaw } from 'draft-js'
import { getFirebase } from 'react-redux-firebase'
import SpaceFormReducer, { initialState } from './SpaceFormReducer'
import { setFieldValue, setLocalizedFieldValue, setCheckingValidation, setLoading, resetSpaceError }  from './SpaceFormActions'
import { statusOptions, spaceTypeOptions, spaceActivityOptions, spaceAppointmentOptions } from 'lib/constants'
import { ParseEditorContent, emptyEditorState, renderCommaList, parseCommaList } from 'lib/utils'
import { transformDatepickerValue } from 'lib/datetime'

const SpaceForm = ({ initialFormValues, spaceId, history, related }) => {
  // useReducer is a new react way to use a custom local reducer instead of the usual
  // local state. In this case, SpaceFormReducer is our reducer.
  const [formState, dispatch] = useReducer(
    SpaceFormReducer,  // Our Reducer
    initialState,
    () => { // This third parameter overrites our initialstaate
      if (spaceId) {
        // NEED TO PARSE EDITOR CONTENT FROM INITIAL FORM VALUES
        return {
          ...initialState,
          formValues: {
            ...initialFormValues,
            localizedContent: {
              en: {
                ...initialFormValues.localizedContent.en,
                mainContent: ParseEditorContent(initialFormValues.localizedContent.en.mainContent)
              },
              es: {
                ...initialFormValues.localizedContent.es,
                mainContent: ParseEditorContent(initialFormValues.localizedContent.es.mainContent)
              }
            }
          }
        }
      } else {
        return initialState
      }
    }
  )

  const onSubmitForm = (event) => {
    const form = event.currentTarget

    event.preventDefault()
    event.stopPropagation()

    if (form.checkValidity() === true) {
      submitSpaceForm(formValues)
    }

    // start checking validation on the form now we have initial values
    dispatch(setCheckingValidation())
  }

  const submitSpaceForm = (formValues) => {
    const firebase = getFirebase()

    dispatch(resetSpaceError())
    dispatch(setLoading(true))

    let docRef = firebase.firestore().collection('spaces').doc()

    // Check new or edit
    if (spaceId) {
      docRef = firebase.firestore().collection('spaces').doc(spaceId)
    }

    let hoursTransformed = {};
    Object.keys(formValues.hours).forEach((key) => {
      hoursTransformed[key] = [
        transformDatepickerValue(formValues.hours[key][0]),
        transformDatepickerValue(formValues.hours[key][1])
      ]
    })

    docRef.set({
      createdDate: new Date().getTime(),
      ...formValues,
      updatedDate: new Date().getTime(),
      // REFACTOR
      localizedContent: {
        es: {
          ...formValues.localizedContent.es,
          mainContent: JSON.stringify(convertToRaw(formValues.localizedContent.es.mainContent.getCurrentContent())),
        },
        en: {
          ...formValues.localizedContent.en,
          mainContent: JSON.stringify(convertToRaw(formValues.localizedContent.en.mainContent.getCurrentContent()))
        }
      },
      hours: hoursTransformed
    })
      .then(() => {
        dispatch(setLoading(false))
        if (!spaceId) {
          history.push(`/space/edit/${docRef.id}`)
        }
        // this is where we can trigger a notification or something
      })
      .catch( error => {
        // Error ocurred
        dispatch(setLoading(false))
        // dispatch(setSpaceError(error))
      })
  }

  const removeSpace = (spaceId) => {
    const firebase = getFirebase()

    if (spaceId) {
      // Confirm with user
      const confirmed = window.confirm('Are you sure you want to remove this space?')

      if(confirmed) {
        // Delete document
        firebase.firestore().collection('spaces').doc(spaceId).delete()
          .then(() => {
            // Redirect to /spaces
            history.push(`/spaces`)
            // this is where we can trigger a notification or something
          })
          .catch( error => {
            // Error ocurred
            dispatch(setLoading(false))
          })
      }
    }
  }

  const {
    formValues,
    error,
    loading,
    isCheckingValidation,
  } = formState

  const {
    name,
    localizedContent,
    coverImage,
    location,
    phone,
    email,
    website,
    instagram,
    hours,
    appointment,
    status,
    active,
    type,
    keywords,
    relatedTo,
    relatedExternal,
    languages,
    bookmarkCount,
    bookmarks,
  } = formValues

  const { en, es } = localizedContent

  return (
    <Container>
      <h2>{spaceId ? 'Space' : 'New Space'}</h2>

      <Form
        noValidate
        validated={isCheckingValidation}
        onSubmit={event => onSubmitForm(event)}
      >
        <Row>
          <Col md={8}>
            <Row>
              <Col md={12}>
                <Form.Group controlId='name'>
                  <Form.Label>Name</Form.Label>
                  <Form.Control type='text' value={name} onChange={ e => dispatch(setFieldValue('name', e.target.value)) } disabled={loading} required />
                  <Form.Control.Feedback type='invalid'>Please provide a valid name.</Form.Control.Feedback>
                </Form.Group>
              </Col>

            </Row>
            <Row>

              <Col md={6}>
                <Form.Group controlId='phone'>
                  <Form.Label>Phone</Form.Label>
                  <Form.Control type='text' value={phone} onChange={ e => dispatch(setFieldValue('phone', e.target.value)) } disabled={loading} />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group controlId='email'>
                  <Form.Label>Email</Form.Label>
                  <Form.Control type='text' value={email} onChange={ e => dispatch(setFieldValue('email', e.target.value)) } disabled={loading} />
                </Form.Group>
              </Col>


            </Row>
            <Row>

              <Col md={6}>
                <Form.Group controlId='website'>
                  <Form.Label>Website</Form.Label>
                  <Form.Control type='text' value={website} onChange={ e => dispatch(setFieldValue('website', e.target.value)) } disabled={loading} />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group controlId='instagram'>
                  <Form.Label>Instagram Handle</Form.Label>
                  <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                      <InputGroup.Text id="handle-at">@</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control type='text' value={instagram} onChange={ e => dispatch(setFieldValue('instagram', e.target.value)) } disabled={loading} />
                  </InputGroup>
                </Form.Group>
              </Col>

            </Row>

            <HoursField
              onChange={ value => dispatch(setFieldValue('hours', value)) }
              value={hours}
            />

            <Row>
              <Col md={6}>

                <Form.Group controlId='appointment'>
                  <Form.Label>Appointments</Form.Label>
                  <Form.Control as='select' onChange={ e => dispatch(setFieldValue('appointment', e.target.value)) } defaultValue={appointment} disabled={loading}>
                    { Object.keys(spaceAppointmentOptions).map( key => <option value={key} key={key}>{spaceAppointmentOptions[key]}</option> ) }
                  </Form.Control>
                </Form.Group>

              </Col>
            </Row>

            <Row>

              <Col md={6}>
                <Form.Group controlId='es-content'>
                  <Form.Label>Content Español</Form.Label>
                  <RichEditor editorState={es.mainContent} onEditorStateChange={editorState => { dispatch(setLocalizedFieldValue('mainContent', 'es', editorState)) }} />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group controlId='en-content'>
                  <Form.Label>Content English</Form.Label>
                  <RichEditor editorState={en.mainContent} onEditorStateChange={editorState => { dispatch(setLocalizedFieldValue('mainContent', 'en', editorState)) }} />
                </Form.Group>
              </Col>

            </Row>

            <RelatedInternalContent
              docId={spaceId}
              loading={loading}
              related={related}
              collection={'spaces'}
            />

            <LocationField locationState={location} loading={loading} onLocationStateChange={location => { dispatch(setFieldValue('location', location)) }} />

          </Col>

          <Col>
            <Form.Group controlId='actions'>
              <Form.Label>Actions</Form.Label>
              <Row className='justify-content-between'>
                <Col>
                  <Button variant='primary' type='submit' disabled={loading} block>
                    { loading ? 'Loading…' : 'Save' }
                  </Button>
                </Col>
                { spaceId &&
                  <Col xs='auto'>
                    <Button disabled={loading} variant='danger' onClick={() => removeSpace(spaceId)} block>Remove</Button>
                  </Col>
                }
              </Row>
            </Form.Group>

            <Form.Group controlId='status'>
              <Form.Label>Status</Form.Label>
              <Form.Control as='select' onChange={ e => dispatch(setFieldValue('status', e.target.value)) } defaultValue={status} disabled={loading}>
                { Object.keys(statusOptions).map( key => (
                  <option value={key} key={key}>{statusOptions[key]}</option>
                )) }
              </Form.Control>
            </Form.Group>

            <Form.Group controlId='type'>
              <Form.Label>Type</Form.Label>
              <Form.Control as='select' onChange={ e => dispatch(setFieldValue('type', e.target.value)) } defaultValue={type} disabled={loading}>
                { Object.keys(spaceTypeOptions).map( key => <option value={key} key={key}>{spaceTypeOptions[key]}</option> ) }
              </Form.Control>
            </Form.Group>

            <Languages value={languages} onChange={ value => dispatch(setFieldValue('languages', value)) } />

            <Form.Group controlId='active'>
              <Form.Label>Active</Form.Label>
              <Form.Control as='select' onChange={ e => {
                const boolValue = e.target.value === 'true' ? true : false
                dispatch(setFieldValue('active', boolValue))
              }} defaultValue={active} disabled={loading}>
                <option value={'true'}>True</option>
                <option value={'false'}>False</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId='keywords'>
              <Form.Label>Keywords</Form.Label>
              <Form.Control as='textarea' rows='3' value={renderCommaList(keywords)} onChange={ e => dispatch(setFieldValue('keywords', parseCommaList(e.target.value))) }/>
            </Form.Group>

            <MediaPickerContainer value={coverImage} handleOnChange={ value => dispatch(setFieldValue('coverImage', value))} PickerComponent={MediaPicker} label={'Cover Image'} />

            <Form.Group controlId='es-seo-title'>
              <Form.Label>SEO Title Español</Form.Label>
              <Form.Control value={es.seoTitle} onChange={ e => dispatch(setLocalizedFieldValue('seoTitle','es', e.target.value))} />
            </Form.Group>

            <Form.Group controlId='en-seo-title'>
              <Form.Label>SEO Title English</Form.Label>
              <Form.Control value={en.seoTitle} onChange={ e => dispatch(setLocalizedFieldValue('seoTitle','en', e.target.value))} />
            </Form.Group>

            <Form.Group controlId='es-seo-description'>
              <Form.Label>SEO Description Español</Form.Label>
              <Form.Control as='textarea' rows='3' value={es.seoDescription} onChange={ e => dispatch(setLocalizedFieldValue('seoDescription','es', e.target.value))} />
            </Form.Group>

            <Form.Group controlId='en-seo-description'>
              <Form.Label>SEO Description English</Form.Label>
              <Form.Control as='textarea' rows='3' value={en.seoDescription} onChange={ e => dispatch(setLocalizedFieldValue('seoDescription','en', e.target.value))} />
            </Form.Group>

          </Col>
        </Row>
      </Form>
    </Container>
  )
}

SpaceForm.propTypes = {
  formState: PropTypes.object,
  initialFormState: PropTypes.object,
}

export default withRouter(SpaceForm)
