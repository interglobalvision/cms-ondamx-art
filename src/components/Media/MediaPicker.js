import React, { useState, useReducer } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import { isLoaded, isEmpty } from 'react-redux-firebase'

import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'

import MediaLibraryModal from 'components/Media/MediaLibraryModal'

const MediaPicker = ({ media, mediaPage, history, value, handleOnChange, totalPages, currentPage, label }) => {
  const [showModal, setShowModal] = useState(false);

  if (media && value) {
    const mediaMissing = media.filter(item => item.id === value.id).length === 0
    if (mediaMissing) {
      // media item not found in database
      handleOnChange(null)
    }
  }

  return (
    <>
      <Row className='justify-content-between'>
        {label &&
          <Col xs='12'>
            <label className='form-label'>{label}</label>
          </Col>
        }
        <Col xs='12' onClick={() => { setShowModal(true) }}>
          {!value ? (
            <div className={'dropzone-container'}>Click here to select</div>
          ) : (
            <div className='form-group'>
              <Image className={'u-pointer'} src={value.mediaUrl} thumbnail />
            </div>
          )}
        </Col>
      </Row>

      <MediaLibraryModal
        showModal={showModal}
        setShowModal={setShowModal}
        media={media}
        mediaPage={mediaPage}
        totalPages={totalPages}
        currentPage={currentPage}
        handleOnChange={handleOnChange}
      />
    </>
  )
}

MediaPicker.propTypes = {
  media: PropTypes.array,
  mediaPage: PropTypes.array,
  history: PropTypes.object.isRequired,
}

export default compose(
  withRouter,
)(MediaPicker)
