import {
  ARTICLE_FORM_SET_FIELD_VALUE,
  ARTICLE_FORM_SET_LOCALIZED_FIELD_VALUE,
  ARTICLE_FORM_SET_CHECKING_VALIDATION,
  ARTICLE_FORM_SET_ERROR,
  ARTICLE_FORM_RESET_ERROR,
  ARTICLE_FORM_SET_LOADING,
  ARTICLE_FORM_LOAD_DATA,
  ARTICLE_FORM_RESET,
} from './ArticleFormReducer'

export const setFieldValue = (field, data) => {
  return {
    type: ARTICLE_FORM_SET_FIELD_VALUE,
    field,
    data,
  }
}

export const setLocalizedFieldValue = (field, locale, data) => {
  return {
    type: ARTICLE_FORM_SET_LOCALIZED_FIELD_VALUE,
    field,
    locale,
    data,
  }
}

export const setCheckingValidation = () => {
  return {
    type: ARTICLE_FORM_SET_CHECKING_VALIDATION,
  }
}

export const setArticleError = (error = null) => {
  return {
    type: ARTICLE_FORM_SET_ERROR,
    data: error,
  }
}

export const resetArticleError = () => {
  return {
    type: ARTICLE_FORM_RESET_ERROR,
  }
}

export const setLoading = loading => {
  return {
    type: ARTICLE_FORM_SET_LOADING,
    data: loading,
  }
}

export const loadArticleData = data => {
  return {
    type: ARTICLE_FORM_LOAD_DATA,
    data,
  }
}

export const resetArticleForm = () => {
  return {
    type: ARTICLE_FORM_RESET,
  }
}
