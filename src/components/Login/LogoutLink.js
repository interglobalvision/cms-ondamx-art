import React from 'react'
import PropTypes from 'prop-types'
import { logout }  from 'actions/LoginActions'
import { connect } from 'react-redux'
import { compose } from 'redux'

const LogoutLink = ({ Element = 'a', logout, children }) => {
  return (
    <Element
      onClick={ e => {
        e.preventDefault()
        logout()
      }}
    >
      {children}
    </Element>
  )
}

LogoutLink.propTypes = {
  Element: PropTypes.object,
  logout: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired,
}

const mapDipatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
})

export default compose(
  connect(
    null,
    mapDipatchToProps
  )
)(LogoutLink)
