import {
  USER_FORM_SET_FIELD_VALUE,
  USER_FORM_SET_LOCALIZED_FIELD_VALUE,
  USER_FORM_SET_CHECKING_VALIDATION,
  USER_FORM_SET_ERROR,
  USER_FORM_RESET_ERROR,
  USER_FORM_SET_LOADING,
  USER_FORM_LOAD_DATA,
  USER_FORM_RESET,
} from './UserFormReducer'

export const setFieldValue = (field, data) => {
  return {
    type: USER_FORM_SET_FIELD_VALUE,
    field,
    data,
  }
}

export const setLocalizedFieldValue = (field, locale, data) => {
  return {
    type: USER_FORM_SET_LOCALIZED_FIELD_VALUE,
    field,
    locale,
    data,
  }
}

export const setCheckingValidation = () => {
  return {
    type: USER_FORM_SET_CHECKING_VALIDATION,
  }
}

export const setUserError = (error = null) => {
  return {
    type: USER_FORM_SET_ERROR,
    data: error,
  }
}

export const resetUserError = () => {
  return {
    type: USER_FORM_RESET_ERROR,
  }
}

export const setLoading = loading => {
  return {
    type: USER_FORM_SET_LOADING,
    data: loading,
  }
}

export const loadUserData = data => {
  return {
    type: USER_FORM_LOAD_DATA,
    data,
  }
}

export const resetUserForm = () => {
  return {
    type: USER_FORM_RESET,
  }
}
