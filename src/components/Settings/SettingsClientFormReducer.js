import { generatePerLocale } from 'lib/locales'
import { ParseEditorContent, emptyEditorState } from 'lib/utils'

export const SETTINGS_FORM_SET_FIELD_VALUE = 'SETTINGS_FORM_SET_FIELD_VALUE'
export const SETTINGS_FORM_SET_LOCALIZED_FIELD_VALUE = 'SETTINGS_FORM_SET_LOCALIZED_FIELD_VALUE'
export const SETTINGS_FORM_SET_CHECKING_VALIDATION = 'SETTINGS_FORM_SET_CHECKING_VALIDATION'
export const SETTINGS_FORM_SET_ERROR = 'SETTINGS_FORM_SET_ERROR'
export const SETTINGS_FORM_RESET_ERROR = 'SETTINGS_FORM_RESET_ERROR'
export const SETTINGS_FORM_SET_LOADING = 'SETTINGS_FORM_SET_LOADING'
export const SETTINGS_FORM_RESET = 'SETTINGS_FORM_RESET'
export const SETTINGS_FORM_LOAD_DATA = 'SETTINGS_FORM_LOAD_DATA'

// Post initial state
const postContentInitialState = {
  featuredIntro: '',
  featuredCover: {},
}

// This is our initial state
export const initialState = {
  formValues: {
    localizedContent: generatePerLocale(postContentInitialState),
  },
  isCheckingValidation: false,
  error: false,
  loading: false,
}

const SettingsClientFormReducer = (state = initialState, action) => {
  switch (action.type) {
    case SETTINGS_FORM_SET_FIELD_VALUE:
      const { formValues } = state
      return {
        ...state,
        formValues: {
          ...formValues,
          [action.field]: action.data,
        }
      }
    case SETTINGS_FORM_SET_LOCALIZED_FIELD_VALUE:
      return {
        ...state,
        formValues: {
          ...state.formValues,
          localizedContent: localizedContentReducer(state.formValues.localizedContent, action)
        }
      }
    case SETTINGS_FORM_SET_CHECKING_VALIDATION:
      return {
        ...state,
        isCheckingValidation: true,
      }
    case SETTINGS_FORM_SET_ERROR: {
      return {
        ...state,
        error: action.data || false,
      }
    }
    case SETTINGS_FORM_RESET_ERROR: {
      return {
        ...state,
        error: false,
      }
    }
    case SETTINGS_FORM_SET_LOADING: {
      return {
        ...state,
        loading: action.data,
      }
    }
    case SETTINGS_FORM_RESET: {
      return initialState
    }
    default:
      return state
  }
}

const localizedContentReducer = (state = generatePerLocale(postContentInitialState), action) => {
  switch (action.type) {
    case SETTINGS_FORM_SET_LOCALIZED_FIELD_VALUE:
      return {
        ...state,
        [action.locale]: {
          ...state[action.locale],
          [action.field]: action.data,
        }
      }
    default:
      return state
  }
}

export default SettingsClientFormReducer
