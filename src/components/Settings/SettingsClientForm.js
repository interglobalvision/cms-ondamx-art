import React, { useReducer } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'
import DatePicker from 'components/Fields/DatePicker'
import RichEditor from 'components/Fields/RichEditor/RichEditor'
import MediaPicker from 'components/Media/MediaPicker.js'
import MediaPickerContainer from 'containers/MediaPickerContainer'
import { convertToRaw } from 'draft-js'
import { getFirebase } from 'react-redux-firebase'
import SettingsClientFormReducer, { initialState } from './SettingsClientFormReducer'
import { setFieldValue, setLocalizedFieldValue, setCheckingValidation, setLoading, resetSettingsError }  from './SettingsClientFormActions'
import { ParseEditorContent, emptyEditorState, renderCommaList, parseCommaList } from 'lib/utils'
import { transformDatepickerValue } from 'lib/datetime'

const SettingsClientForm = ({ initialFormValues, settingsId, history }) => {
  // useReducer is a new react way to use a custom local reducer instead of the usual
  // local state. In this case, SettingsFormReducer is our reducer.
  const [formState, dispatch] = useReducer(
    SettingsClientFormReducer,  // Our Reducer
    initialState,
    () => { // This third parameter overrites our initialstaate
      if (settingsId) {
        // NEED TO PARSE EDITOR CONTENT FROM INITIAL FORM VALUES
        return {
          ...initialState,
          formValues: {
            ...initialFormValues,
            localizedContent: {
              en: {
                ...initialFormValues.localizedContent.en,
                featuredIntro: ParseEditorContent(initialFormValues.localizedContent.en.featuredIntro)
              },
              es: {
                ...initialFormValues.localizedContent.es,
                featuredIntro: ParseEditorContent(initialFormValues.localizedContent.es.featuredIntro)
              }
            }
          }
        }
      } else {
        return initialState
      }
    }
  )

  const onSubmitForm = (event) => {
    const form = event.currentTarget

    event.preventDefault()
    event.stopPropagation()

    if (form.checkValidity() === true) {
      submitSettingsForm(formValues)
    }

    // start checking validation on the form now we have initial values
    dispatch(setCheckingValidation())
  }

  const submitSettingsForm = (formValues) => {
    const firebase = getFirebase()

    dispatch(resetSettingsError())
    dispatch(setLoading(true))

    let docRef = firebase.firestore().collection('settings').doc(settingsId)

    docRef.set({
      createdDate: new Date().getTime(),
      ...formValues,
      updatedDate: new Date().getTime(),
      // REFACTOR
      localizedContent: {
        es: {
          ...formValues.localizedContent.es,
          featuredIntro: JSON.stringify(convertToRaw(formValues.localizedContent.es.featuredIntro.getCurrentContent())),
        },
        en: {
          ...formValues.localizedContent.en,
          featuredIntro: JSON.stringify(convertToRaw(formValues.localizedContent.en.featuredIntro.getCurrentContent()))
        }
      },
      // convert date values to explicit CDMX timestamps
      publishDate: transformDatepickerValue(formValues.publishDate),
      //Convert editorState to Raw content
    })
      .then(() => {
        dispatch(setLoading(false))
        // this is where we can trigger a notification or something
      })
      .catch( error => {
        // Error ocurred
        dispatch(setLoading(false))
        // dispatch(setSettingsError(error))
      })
  }

  const {
    formValues,
    error,
    loading,
    isCheckingValidation,
  } = formState

  const {
    localizedContent,
  } = formValues

  const { en, es } = localizedContent

  return (
    <Container>
      <h2>Client Settings</h2>

      <Form
        noValidate
        validated={isCheckingValidation}
        onSubmit={event => onSubmitForm(event)}
      >
        <Row>
          <Col md={8}>

            <Row>
              <Col md={6}>
                <MediaPickerContainer value={es.featuredCover} handleOnChange={ value => dispatch(setLocalizedFieldValue('featuredCover', 'es', value))} PickerComponent={MediaPicker} label={'Featured Cover Español'} />
              </Col>
              <Col md={6}>
                <MediaPickerContainer value={en.featuredCover} handleOnChange={ value => dispatch(setLocalizedFieldValue('featuredCover', 'en', value))} PickerComponent={MediaPicker} label={'Featured Cover English'} />
              </Col>
            </Row>

            <Row>
              <Col md={6}>
                <Form.Group controlId='es-content'>
                  <Form.Label>Featured Intro Español</Form.Label>
                  <RichEditor
                    editorState={es.featuredIntro}
                    onEditorStateChange={editorState => {
                      dispatch(setLocalizedFieldValue('featuredIntro', 'es', editorState))
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group controlId='en-content'>
                  <Form.Label>Featured Intro English</Form.Label>
                  <RichEditor
                    editorState={en.featuredIntro}
                    onEditorStateChange={editorState => {
                      dispatch(setLocalizedFieldValue('featuredIntro', 'en', editorState))
                    }}
                  />
                </Form.Group>
              </Col>
            </Row>
            
          </Col>

          <Col>
            <Form.Group controlId='actions'>
              <Form.Label>Actions</Form.Label>
              <Row className='justify-content-between'>
                <Col>
                  <Button variant='primary' type='submit' disabled={loading} block>
                    { loading ? 'Loading…' : 'Save' }
                  </Button>
                </Col>
              </Row>
            </Form.Group>

          </Col>
        </Row>
      </Form>
    </Container>
  )
}

SettingsClientForm.propTypes = {
  formState: PropTypes.object,
  initialFormState: PropTypes.object,
}

export default withRouter(SettingsClientForm)
