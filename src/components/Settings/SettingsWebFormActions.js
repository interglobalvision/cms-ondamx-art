import {
  SETTINGS_FORM_SET_FIELD_VALUE,
  SETTINGS_FORM_SET_LOCALIZED_FIELD_VALUE,
  SETTINGS_FORM_SET_CHECKING_VALIDATION,
  SETTINGS_FORM_SET_ERROR,
  SETTINGS_FORM_RESET_ERROR,
  SETTINGS_FORM_SET_LOADING,
  SETTINGS_FORM_LOAD_DATA,
  SETTINGS_FORM_RESET,
} from './SettingsWebFormReducer'

export const setFieldValue = (field, data) => {
  return {
    type: SETTINGS_FORM_SET_FIELD_VALUE,
    field,
    data,
  }
}

export const setLocalizedFieldValue = (field, locale, data) => {
  return {
    type: SETTINGS_FORM_SET_LOCALIZED_FIELD_VALUE,
    field,
    locale,
    data,
  }
}

export const setCheckingValidation = () => {
  return {
    type: SETTINGS_FORM_SET_CHECKING_VALIDATION,
  }
}

export const setSettingsError = (error = null) => {
  return {
    type: SETTINGS_FORM_SET_ERROR,
    data: error,
  }
}

export const resetSettingsError = () => {
  return {
    type: SETTINGS_FORM_RESET_ERROR,
  }
}

export const setLoading = loading => {
  return {
    type: SETTINGS_FORM_SET_LOADING,
    data: loading,
  }
}

export const loadSettingsData = data => {
  return {
    type: SETTINGS_FORM_LOAD_DATA,
    data,
  }
}

export const resetSettingsForm = () => {
  return {
    type: SETTINGS_FORM_RESET,
  }
}
