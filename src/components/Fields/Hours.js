import React, { Component } from 'react'
import Day from 'components/Fields/HoursDay'

export default class Hours extends Component {
  constructor(props) {
    super(props);

    const { value } = props
    this.state = {
      hours: value !== undefined ? value : {
        monday: [null, null],
        tuesday: [null, null],
        wednesday: [null, null],
        thursday: [null, null],
        friday: [null, null],
        saturday: [null, null],
        sunday: [null, null],
      }
    }
  }

  handleDateChange(date, day, index) {
    let dayValue = this.state.hours[day]
    dayValue[index] = date

    this.setState(prevState => ({
      hours: {
        ...prevState.hours,
        [day]: dayValue
      }
    }))

    this.props.onChange(this.state.hours);
  }

  render() {
    return (
      <>
        <Day
          day={'Monday'}
          value={this.state.hours.monday}
          handleChange={ (date, index) => this.handleDateChange(date, 'monday', index) }
        />
        <Day
          day={'Tuesday'}
          value={this.state.hours.tuesday}
          handleChange={ (date, index) => this.handleDateChange(date, 'tuesday', index) }
        />
        <Day
          day={'Wednesday'}
          value={this.state.hours.wednesday}
          handleChange={ (date, index) => this.handleDateChange(date, 'wednesday', index) }
        />
        <Day
          day={'Thursday'}
          value={this.state.hours.thursday}
          handleChange={ (date, index) => this.handleDateChange(date, 'thursday', index) }
        />
        <Day
          day={'Friday'}
          value={this.state.hours.friday}
          handleChange={ (date, index) => this.handleDateChange(date, 'friday', index) }
        />
        <Day
          day={'Saturday'}
          value={this.state.hours.saturday}
          handleChange={ (date, index) => this.handleDateChange(date, 'saturday', index) }
        />
        <Day
          day={'Sunday'}
          value={this.state.hours.sunday}
          handleChange={ (date, index) => this.handleDateChange(date, 'sunday', index) }
        />
      </>
    )
  }
}
