import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Link } from 'react-router-dom'
import { createModalPagination } from 'lib/utils'

const ModalPagination = ({ totalPages, currentPage, handlePageChange }) => {
  return (
    <Row className='justify-content-center'>
      <Col xs='auto'>
        {currentPage !== 0 ? (
          <a href='#' onClick={() => { handlePageChange(currentPage - 1) }}>Previous</a>
        ) : (
          <span>Previous</span>
        )}
      </Col>

      {createModalPagination(totalPages, currentPage, handlePageChange)}

      <Col xs='auto'>
        {currentPage < (totalPages - 1) ? (
          <a href='#' onClick={() => { handlePageChange(currentPage + 1) }} >Next</a>
        ) : (
          <span>Next</span>
        )}
      </Col>
    </Row>
  )
}

export default ModalPagination
