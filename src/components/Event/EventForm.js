import React, { useReducer } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'
import RichEditor from 'components/Fields/RichEditor/RichEditor'
import MediaCarousel from 'components/Fields/MediaCarousel.js'
import DatePicker from 'components/Fields/DatePicker'
import HoursField from 'components/Fields/Hours'
import Languages from 'components/Fields/Languages'
import RelatedExternalLink from 'components/Fields/RelatedExternalLink'
import RelatedInternalContent from 'components/Fields/RelatedInternalContent'
import MediaPicker from 'components/Media/MediaPicker.js'
import LocationField from 'components/Fields/Location'
import EventSpace from 'components/Fields/EventSpace'
import MediaPickerContainer from 'containers/MediaPickerContainer'
import { convertToRaw } from 'draft-js'
import { getFirebase } from 'react-redux-firebase'
import EventFormReducer, { initialState } from './EventFormReducer'
import { setFieldValue, setLocalizedFieldValue, setCheckingValidation, setLoading, resetEventError }  from './EventFormActions'
import { statusOptions, eventTypeOptions, highlightOptions, spaceAppointmentOptions } from 'lib/constants'
import { ParseEditorContent, emptyEditorState, renderCommaList, parseCommaList } from 'lib/utils'
import { transformDatepickerValue } from 'lib/datetime'
import { removeNil } from '../../lib/utils'

const EventForm = ({ initialFormValues, eventId, history, spaces, related }) => {
  // useReducer is a new react way to use a custom local reducer instead of the usual
  // local state. In this case, EventFormReducer is our reducer.
  const [formState, dispatch] = useReducer(
    EventFormReducer,  // Our Reducer
    initialState,
    () => { // This third parameter overrites our initialstaate
      if (eventId) {
        // NEED TO PARSE EDITOR CONTENT FROM INITIAL FORM VALUES
        return {
          ...initialState,
          formValues: {
            ...initialFormValues,
            localizedContent: {
              en: {
                ...initialFormValues.localizedContent.en,
                mainContent: ParseEditorContent(initialFormValues.localizedContent.en.mainContent)
              },
              es: {
                ...initialFormValues.localizedContent.es,
                mainContent: ParseEditorContent(initialFormValues.localizedContent.es.mainContent)
              }
            }
          }
        }
      } else {
        return initialState
      }
    }
  )

  const onSubmitForm = (event) => {
    const form = event.currentTarget

    event.preventDefault()
    event.stopPropagation()

    if (form.checkValidity() === true) {
      submitEventForm(formValues)
    }

    // start checking validation on the form now we have initial values
    dispatch(setCheckingValidation())
  }

  const submitEventForm = (formValues) => {
    const firebase = getFirebase()

    dispatch(resetEventError())
    dispatch(setLoading(true))

    let docRef = firebase.firestore().collection('events').doc()
    let slug = formValues.slug

    // Check new or edit
    if (eventId) {
      docRef = firebase.firestore().collection('events').doc(eventId)
    }

    if (slug === undefined || slug === '' || slug.includes('_')) {
      const slugTitle = formValues.localizedContent.es.name ? formValues.localizedContent.es.name : formValues.localizedContent.en.name
      slug = slugTitle ? slugTitle.toLowerCase().replace(/[^A-Z0-9]+/ig, '-') : ''
    }

    docRef.set(removeNil({
      createdDate: new Date().getTime(),
      ...formValues,
      slug,
      updatedDate: new Date().getTime(),
      // REFACTOR
      localizedContent: {
        es: {
          ...formValues.localizedContent.es,
          mainContent: JSON.stringify(convertToRaw(formValues.localizedContent.es.mainContent.getCurrentContent())),
        },
        en: {
          ...formValues.localizedContent.en,
          mainContent: JSON.stringify(convertToRaw(formValues.localizedContent.en.mainContent.getCurrentContent()))
        }
      },
      // convert date values to explicit CDMX timestamps
      openingStart: transformDatepickerValue(formValues.openingStart),
      openingEnd: transformDatepickerValue(formValues.openingEnd),
      closing: transformDatepickerValue(formValues.closing),
      //Convert editorState to Raw content
    }))
      .then(() => {
        dispatch(setLoading(false))
        if (!eventId) {
          history.push(`/event/edit/${docRef.id}`)
        }
        // this is where we can trigger a notification or something
      })
      .catch( error => {
        // Error ocurred
        dispatch(setLoading(false))
        // dispatch(setEventError(error))
      })
  }

  const removeEvent = (eventId) => {
    const firebase = getFirebase()

    if (eventId) {
      // Confirm with user
      const confirmed = window.confirm('Are you sure you want to remove this event?')

      if(confirmed) {
        // Delete document
        firebase.firestore().collection('events').doc(eventId).delete()
          .then(() => {
            // Redirect to /events
            history.push(`/events`)
            // this is where we can trigger a notification or something
          })
          .catch( error => {
            // Error ocurred
            dispatch(setLoading(false))
          })
      }
    }
  }

  const {
    formValues,
    error,
    loading,
    isCheckingValidation,
  } = formState

  const {
    localizedContent,
    imageCarousel,
    coverImage,
    space,
    location,
    hours,
    appointment,
    openingStart,
    openingEnd,
    hasOpening,
    multiDay,
    closing,
    ticketUrl,
    status,
    highlight,
    featuredOrder,
    type,
    artists,
    keywords,
    relatedTo,
    relatedExternal,
    languages,
    bookmarkCount,
    bookmarks,
    slug,
  } = formValues

  const { en, es } = localizedContent

  return (
    <Container>
      <h2>{eventId ? 'Event' : 'New Event'}</h2>

      <Form
        noValidate
        validated={isCheckingValidation}
        onSubmit={event => onSubmitForm(event)}
      >
        <Row>
          <Col md={8}>
            <Row>
              <Col md={6}>
                <Form.Group controlId='es-name'>
                  <Form.Label>Name Español</Form.Label>
                  <Form.Control type='text' value={es.name} onChange={ e => dispatch(setLocalizedFieldValue('name', 'es', e.target.value)) } disabled={loading} required />
                  <Form.Control.Feedback type='invalid'>Please provide a valid name.</Form.Control.Feedback>
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group controlId='en-name'>
                  <Form.Label>Name English</Form.Label>
                  <Form.Control type='text' value={en.name} onChange={ e => dispatch(setLocalizedFieldValue('name','en', e.target.value)) } disabled={loading} required />
                  <Form.Control.Feedback type='invalid'>Please provide a valid name.</Form.Control.Feedback>
                </Form.Group>
              </Col>

            </Row>
            <Row>

              <Col md={6}>
                <Form.Group controlId='es-title'>
                  <Form.Label>Title Español</Form.Label>
                  <Form.Control type='text' value={es.title} onChange={ e => dispatch(setLocalizedFieldValue('title', 'es', e.target.value)) } disabled={loading} />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group controlId='en-title'>
                  <Form.Label>Title English</Form.Label>
                  <Form.Control type='text' value={en.title} onChange={ e => dispatch(setLocalizedFieldValue('title','en', e.target.value)) } disabled={loading} />
                </Form.Group>
              </Col>

            </Row>
            <Row>

              <Col md={12}>
                <Form.Group controlId='artists'>
                  <Form.Label>Artists</Form.Label>
                  <Form.Control as='textarea' rows='2' value={renderCommaList(artists)} onChange={ e => dispatch(setFieldValue('artists', parseCommaList(e.target.value))) } disabled={loading} />
                </Form.Group>
              </Col>

            </Row>
            <Row>

              <Col md={6}>
                <Form.Group controlId='openingStart'>
                  <Form.Label>Opening Start</Form.Label>
                  <DatePicker
                    selected={openingStart}
                    onChange={ date => dispatch(setFieldValue('openingStart', date)) }
                    showTimeSelect
                    timeFormat='HH:mm'
                    timeIntervals={15}
                    dateFormat='d MMMM, yyyy h:mm aa'
                    timeCaption='time'
                    fixedHeight
                    className='form-control'
                    allowSameDay
                    isClearable
                  />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group controlId='openingEnd'>
                  <Form.Label>Opening End</Form.Label>
                  <DatePicker
                    selected={openingEnd}
                    onChange={ date => dispatch(setFieldValue('openingEnd', date)) }
                    showTimeSelect
                    timeFormat='HH:mm'
                    timeIntervals={15}
                    dateFormat='d MMMM, yyyy h:mm aa'
                    timeCaption='time'
                    fixedHeight
                    className='form-control'
                    allowSameDay
                    isClearable
                  />
                </Form.Group>
              </Col>

            </Row>
            <Row>

              <Col md={6}>
                <Form.Group controlId='hasOpening'>
                  <Form.Check
                    custom
                    type='checkbox'
                    id='hasOpening'
                    label={`Event has Opening`}
                    checked={hasOpening}
                    onChange={ e => dispatch(setFieldValue('hasOpening', e.target.checked)) }
                    disabled={loading}
                  />
                </Form.Group>
                <Form.Group controlId='multiDay'>
                  <Form.Check
                    custom
                    type='checkbox'
                    id='multiDay'
                    label={`Multi-Day Event`}
                    checked={multiDay}
                    onChange={ e => dispatch(setFieldValue('multiDay', e.target.checked)) }
                    disabled={loading}
                  />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group controlId='closing'>
                  <Form.Label>Closing</Form.Label>
                  <DatePicker
                    selected={closing}
                    onChange={ date => dispatch(setFieldValue('closing', date)) }
                    showTimeSelect
                    timeFormat='HH:mm'
                    timeIntervals={15}
                    dateFormat='d MMMM, yyyy h:mm aa'
                    timeCaption='time'
                    fixedHeight
                    className='form-control'
                    allowSameDay
                    isClearable
                  />
                </Form.Group>
              </Col>

            </Row>

            <HoursField
              onChange={ value => dispatch(setFieldValue('hours', value)) }
              value={hours}
            />

            <Row>
              <Col md={6}>

                <Form.Group controlId='appointment'>
                  <Form.Label>Appointments</Form.Label>
                  <Form.Control as='select' onChange={ e => dispatch(setFieldValue('appointment', e.target.value)) } defaultValue={appointment} disabled={loading}>
                    { Object.keys(spaceAppointmentOptions).map( key => <option value={key} key={key}>{spaceAppointmentOptions[key]}</option> ) }
                  </Form.Control>
                </Form.Group>

              </Col>
            </Row>

            <MediaPickerContainer value={imageCarousel} handleOnChange={ value => dispatch(setFieldValue('imageCarousel', value))} PickerComponent={MediaCarousel}/>

            <Row>

              <Col md={6}>
                <Form.Group controlId='es-content'>
                  <Form.Label>Content Español</Form.Label>
                  <RichEditor editorState={es.mainContent} onEditorStateChange={editorState => { dispatch(setLocalizedFieldValue('mainContent', 'es', editorState)) }} />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group controlId='en-content'>
                  <Form.Label>Content English</Form.Label>
                  <RichEditor editorState={en.mainContent} onEditorStateChange={editorState => { dispatch(setLocalizedFieldValue('mainContent', 'en', editorState)) }} />
                </Form.Group>
              </Col>

            </Row>
            <Row>

              <Col md={12}>
                <Form.Group controlId='ticketUrl'>
                  <Form.Label>Ticket URL</Form.Label>
                  <Form.Control type='text' value={ticketUrl} onChange={ e => dispatch(setFieldValue('ticketUrl', e.target.value)) } disabled={loading} />
                </Form.Group>
              </Col>

            </Row>

            <RelatedExternalLink
              externalValue={relatedExternal}
              updateValue={(value) => dispatch(setFieldValue('relatedExternal', value))}
              loading={loading}
            />

            <RelatedInternalContent
              docId={eventId}
              loading={loading}
              related={related}
              collection={'events'}
            />

            <LocationField locationState={location} loading={loading} onLocationStateChange={location => { dispatch(setFieldValue('location', location)) }} />

          </Col>

          <Col>
            <Form.Group controlId='actions'>
              <Form.Label>Actions</Form.Label>
              <Row className='justify-content-between'>
                <Col>
                  <Button variant='primary' type='submit' disabled={loading} block>
                    { loading ? 'Loading…' : 'Save' }
                  </Button>
                </Col>
                { eventId &&
                  <Col xs='auto'>
                    <Button disabled={loading} variant='danger' onClick={() => removeEvent(eventId)} block>Remove</Button>
                  </Col>
                }
              </Row>
            </Form.Group>

            {slug !== undefined && slug !== '' &&
              <Form.Group controlId='slug'>
                <Form.Label>Permalink</Form.Label>
                <div><a href={'https://ondamx.art/evento/' + slug + '-' + eventId}>{'https://ondamx.art/evento/' + slug + '-' + eventId}</a></div>
              </Form.Group>
            }

            <Form.Group controlId='status'>
              <Form.Label>Status</Form.Label>
              <Form.Control as='select' onChange={ e => dispatch(setFieldValue('status', e.target.value)) } defaultValue={status} disabled={loading}>
                { Object.keys(statusOptions).map( key => (
                  <option value={key} key={key}>{statusOptions[key]}</option>
                )) }
              </Form.Control>
            </Form.Group>

            <Row>
              <Col xs={highlight === 'featured' ? 8 : 12}>
                <Form.Group controlId='highlight'>
                  <Form.Label>Highlight</Form.Label>
                  <Form.Control as='select' onChange={ e => dispatch(setFieldValue('highlight', e.target.value)) } defaultValue={highlight} disabled={loading}>
                    { Object.keys(highlightOptions).map( key => (
                      <option value={key} key={key}>{highlightOptions[key]}</option>
                    )) }
                  </Form.Control>
                </Form.Group>
              </Col>
              <Col xs={4} className={highlight === 'featured' ? '' : 'u-hidden'}>
                <Form.Group controlId='featuredOrder'>
                  <Form.Label>Order</Form.Label>
                  <Form.Control type='number' min={1} value={featuredOrder} onChange={ e => dispatch(setFieldValue('featuredOrder', e.target.value)) } disabled={loading || highlight !== 'featured'} />
                </Form.Group>
              </Col>
            </Row>

            <EventSpace
              externalValue={space}
              updateValue={space => {
                const name = space.name === undefined ? space : space.name

                let value = {
                  name
                }
                if (space.id !== undefined) {
                  value['id'] = space.id

                  if (
                    location.lat === 0 &&
                    location.lon === 0 &&
                    (location.address.street === undefined || location.address.street === '')
                  ) {
                    dispatch(setFieldValue('location', space.location))
                  }

                  dispatch(setFieldValue('hours', space.hours))
                  dispatch(setFieldValue('appointment', space.appointment))
                }

                dispatch(setFieldValue('space', value))
              }}
              loading={loading}
              placeholder={'Type a space name'}
            />

            <Form.Group controlId='es-featuredSummary' className={highlight === 'featured' ? '' : 'u-hidden'}>
              <Form.Label>Summary Español</Form.Label>
              <Form.Control as='textarea' rows='3' value={es.featuredSummary} onChange={ e => dispatch(setLocalizedFieldValue('featuredSummary','es', e.target.value))} disabled={loading || highlight !== 'featured'} />
            </Form.Group>

            <Form.Group controlId='en-featuredSummary' className={highlight === 'featured' ? '' : 'u-hidden'}>
              <Form.Label>Summary English</Form.Label>
              <Form.Control as='textarea' rows='3' value={en.featuredSummary} onChange={ e => dispatch(setLocalizedFieldValue('featuredSummary','en', e.target.value))} disabled={loading || highlight !== 'featured'} />
            </Form.Group>

            <Form.Group controlId='type'>
              <Form.Label>Type</Form.Label>
              <Form.Control as='select' onChange={ e => dispatch(setFieldValue('type', e.target.value)) } defaultValue={type} disabled={loading}>
                { Object.keys(eventTypeOptions).map( key => <option value={key} key={key}>{eventTypeOptions[key]}</option> ) }
              </Form.Control>
            </Form.Group>

            <Languages value={languages} onChange={ value => dispatch(setFieldValue('languages', value)) } />

            <Form.Group controlId='keywords'>
              <Form.Label>Keywords</Form.Label>
              <Form.Control as='textarea' rows='3' value={renderCommaList(keywords)} onChange={ e => dispatch(setFieldValue('keywords', parseCommaList(e.target.value))) } disabled={loading} />
            </Form.Group>

            <MediaPickerContainer value={coverImage} handleOnChange={ value => dispatch(setFieldValue('coverImage', value))} PickerComponent={MediaPicker} label={'Cover Image'}/>

            <Form.Group controlId='es-seoTitle'>
              <Form.Label>SEO Title Español</Form.Label>
              <Form.Control value={es.seoTitle} onChange={ e => dispatch(setLocalizedFieldValue('seoTitle','es', e.target.value))} disabled={loading} />
            </Form.Group>

            <Form.Group controlId='en-seoTitle'>
              <Form.Label>SEO Title English</Form.Label>
              <Form.Control value={en.seoTitle} onChange={ e => dispatch(setLocalizedFieldValue('seoTitle','en', e.target.value))} disabled={loading} />
            </Form.Group>

            <Form.Group controlId='es-seoDescription'>
              <Form.Label>SEO Description Español</Form.Label>
              <Form.Control as='textarea' rows='3' value={es.seoDescription} onChange={ e => dispatch(setLocalizedFieldValue('seoDescription','es', e.target.value))} disabled={loading} />
            </Form.Group>

            <Form.Group controlId='en-seoDescription'>
              <Form.Label>SEO Description English</Form.Label>
              <Form.Control as='textarea' rows='3' value={en.seoDescription} onChange={ e => dispatch(setLocalizedFieldValue('seoDescription','en', e.target.value))} disabled={loading} />
            </Form.Group>

          </Col>
        </Row>
      </Form>
    </Container>
  )
}

EventForm.propTypes = {
  formState: PropTypes.object,
  initialFormState: PropTypes.object,
}

export default withRouter(EventForm)
