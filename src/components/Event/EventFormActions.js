import {
  EVENT_FORM_SET_FIELD_VALUE,
  EVENT_FORM_SET_LOCALIZED_FIELD_VALUE,
  EVENT_FORM_SET_CHECKING_VALIDATION,
  EVENT_FORM_SET_ERROR,
  EVENT_FORM_RESET_ERROR,
  EVENT_FORM_SET_LOADING,
  EVENT_FORM_LOAD_DATA,
  EVENT_FORM_RESET,
} from './EventFormReducer'

export const setFieldValue = (field, data) => {
  return {
    type: EVENT_FORM_SET_FIELD_VALUE,
    field,
    data,
  }
}

export const setLocalizedFieldValue = (field, locale, data) => {
  return {
    type: EVENT_FORM_SET_LOCALIZED_FIELD_VALUE,
    field,
    locale,
    data,
  }
}

export const setCheckingValidation = () => {
  return {
    type: EVENT_FORM_SET_CHECKING_VALIDATION,
  }
}

export const setEventError = (error = null) => {
  return {
    type: EVENT_FORM_SET_ERROR,
    data: error,
  }
}

export const resetEventError = () => {
  return {
    type: EVENT_FORM_RESET_ERROR,
  }
}

export const setLoading = loading => {
  return {
    type: EVENT_FORM_SET_LOADING,
    data: loading,
  }
}

export const loadEventData = data => {
  return {
    type: EVENT_FORM_LOAD_DATA,
    data,
  }
}

export const resetEventForm = () => {
  return {
    type: EVENT_FORM_RESET,
  }
}
