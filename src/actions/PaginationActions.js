import {
  SET_MODAL_PAGE,
} from 'reducers/PaginationReducer'

export const setModalPage = (data) => {
  return {
    type: SET_MODAL_PAGE,
    data,
  }
}
